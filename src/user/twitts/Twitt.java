package user.twitts;

import org.jetbrains.annotations.NotNull;

import java.time.*;

public class Twitt {

    private String twitID;
    private String caption ;
    private LocalDateTime date ;

    public String getTwitID() {
        return twitID;
    }

    public String getCaption() {
        return caption;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setCaption(@NotNull String caption) {

        if(caption.length()>140)
            caption = caption.substring(0 , 140) ;

        this.caption = caption;
    }

    public Twitt(){

        date = LocalDateTime.now();

    }
}
