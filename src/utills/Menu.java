package utills;

import java.util.Scanner;
import static utills.UtilMethodes.*;

public class Menu {

    public static void mainMenu() {
        System.out.println("please enter your command :\n");
        System.out.println("Sign up");
        System.out.println("Log in");
        System.out.println("Quit");

        Scanner scanner = new Scanner(System.in);
        String command = scanner.nextLine();
        command = command.trim().toLowerCase().replace(" ", "");

        if (command.equals("signup")) {
            clearScreen();
            signUp();
            return;
        } else if (command.equals("login")) {

            return;
        } else if (command.equals("quit")) {

            return;
        } else {
            clearScreen();
            System.out.format("%s is not recognized \n" , command);
            Menu.mainMenu();
        }
    }//end mainMenu

}
